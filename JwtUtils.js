const jwt = require('jsonwebtoken')
const jwksClient = require('jwks-rsa');

//Auth0 credz
const jwksEndpoint = process.env.JWKS_POINT;

//Retrieve the JWKS from Auth0 Tennant
function retrieveJWKS(kid){
	//Set client details
	const client = jwksClient({
		strictSsl: true, // Default value
		jwksUri: jwksEndpoint
	});
	//Retrieve the JWKS
	return new Promise((resolve, reject) =>
		client.getSigningKey(kid, (err, key) => {
			//console.log(key);
			console.log(err);
		if (err) {
			reject(err);
		}

		const signingKey = key.publicKey || key.rsaPublicKey;
		//console.log(signingKey);
		resolve(signingKey);
		})
	);
}
  
async function getClientID(token){
	try {
		//Pull out the actual JWT
		const jwtoken = token.substr(7);

		//decode the JWT to get kid
		const headerEnd = jwtoken.indexOf(".");
		const header = jwtoken.slice(0, headerEnd);
		const lightDecode = Buffer.from(header, 'base64').toString('ascii');
		let tempresult;

		try {
			tempresult = JSON.parse(lightDecode);
		} catch(err) {
			return {message: "corrupted header token"};
		} 
		
		//Find the KID token in JWT header and retrieve the key
		const kid = tempresult.kid;
		//console.log(kid);
		const key = await retrieveJWKS(kid);
		
		//Verify the JWT
		const result = jwt.verify(jwtoken, key, function(err, decoded) {
			if (err) {
				return err;
			}
			//strip out the sub ID field
			const sub = decoded.sub;
			const res = sub.search('auth0|');
			if (res == -1){
				console.log("Error subID bad format. Check bearer token");
				return 'Error subID bad format. Check bearer token';
			}
			const clientID = sub.substr(6);
			return clientID;
		});
		return result;

	} catch(err) {
		return err
	}
}

module.exports.getClientID = getClientID;