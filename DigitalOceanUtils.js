
const aws = require('aws-sdk');

//Digital Ocean credz
const token = process.env.TOKEN;
const secret = process.env.SECRET;
const bucket = process.env.BUCKET;
const endpoint = process.env.END_POINT;

// Set S3 endpoint to DigitalOcean Spaces
const spacesEndpoint = new aws.Endpoint(endpoint);
const s3 = new aws.S3({
  endpoint: spacesEndpoint,
  accessKeyId: token,
  secretAccessKey: secret
});

function saveBoardFile(boarddata, modelname, filename, userID) {
  const params = {
    Body: boarddata,
    Bucket: bucket,
    Key: `users/${userID}/Boards/${modelname}/${filename}`,
  };

  return new Promise((resolve, reject) => {
    s3.putObject(params, function(err, data) {
      if (err) {
        reject({code: err.code});
			}
      resolve(data);
    })
  });
}

function loadBoardFile(modelName, filename, userID) {
	const params = {
		Bucket: bucket,
		Key: `users/${userID}/Boards/${modelName}/${filename}`,
	};
	return new Promise((resolve, reject) => {
		s3.getObject(params, (err, data) => {
			if (err){
				reject({code: err.code});
			} else {
				const response = JSON.parse(data.Body.toString('utf-8'));
				resolve({ boardData: response });
			}
		});
	});
}

function getModels(userID) {
	const params = {
		Bucket: bucket,
		Prefix: `users/${userID}/Boards/`
	};
	return new Promise((resolve, reject) => {
		s3.listObjectsV2(params, (err, data) => {
			if (err){
				reject({code: err.code});
			}
			const files = [];
			console.log(data.Contents);
			for (i in data.Contents){
				let temp = data.Contents[i].Key;
				let index = temp.indexOf('/', 32);
				console.log(index);
				let lastIndex = temp.indexOf('/', 39);
				console.log(lastIndex);
				let filename = temp.substr(10, 20);
				files.push(filename);
				console.log(filename);
			}
			resolve(files);
		});
	});
}

function getBoardFiles(userID, modelName) {
	const params = {
		Bucket: bucket,
		Prefix: `users/${userID}/Boards/${modelName}/`
	};
	return new Promise((resolve, reject) => {
		s3.listObjectsV2(params, (err, data) => {
			if (err){
				reject({code: err.code});
			}
			const files = [];
			for (i in data.Contents){
				let temp = data.Contents[i].Key;
				let index = temp.lastIndexOf('/');
				let filename = temp.substr(index);
				files.push(filename);
			}
			resolve(files);
		});
	});
}

function deleteBoardFile(modelname, filename, userID) {
	const params = {
		Bucket: bucket,
		Key: `users/${userID}/Boards/${modelname}/${filename}`
	};
	return new Promise((resolve, reject) => {
		s3.deleteObject(params, (err, data) => {
			if (err){
				reject({code: err.code});
			}
			resolve(`${filename} deleted`);
		});
	});
}

module.exports.saveBoardFile = saveBoardFile;
module.exports.loadBoardFile = loadBoardFile;
module.exports.getModels = getModels;
module.exports.getBoardFiles = getBoardFiles;
module.exports.deleteBoardFile = deleteBoardFile;