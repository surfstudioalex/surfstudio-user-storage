'use strict';

const aws = require('aws-sdk');

const { getClientID } = require('./JwtUtils');
const { saveBoardFile, 
        loadBoardFile,
        getModels,
        getBoardFiles,
        deleteBoardFile } = require('./DigitalOceanUtils');


//saves the boardmodel to the users file storage
module.exports.save = async (event, context) => {
  try {
    //retrieve the bearer token and validate it to get clientID
    const userID = await getClientID(event.headers.Authorization);
    if (userID.message !== undefined) {
      return {
        body: JSON.stringify({message: userID.message}),
        headers: {},
        statusCode: 400
      };
    }

    //parse the request body in
    const fileInfo = JSON.parse(event.body);
    if (fileInfo.file == undefined || fileInfo.board == undefined || fileInfo.file == "" || fileInfo.board == "" || fileInfo.model == undefined || fileInfo.model == ""){
      return {
        body: JSON.stringify({message: "File, Model or Board field in request body undefined or empty"}),
        headers: {},
        statusCode: 400
      };
    }
    const boardData = JSON.stringify(fileInfo.board);
    const fileName = fileInfo.file;
    const modelName = fileInfo.model;

    //Save the file
    const result = await saveBoardFile(boardData, modelName, fileName, userID);
    if ("ETag" in result ) {
      return {
        body: JSON.stringify({message: "File uploaded sucessfully"}),
        headers: {},
        statusCode: 200
      };
    }
    return {
      body: JSON.stringify({message: "File upload failed, Internal Server Error, please report"}),
      headers: {},
      statusCode: 500
    };

  } catch(err){
    return {
      body: JSON.stringify({message: "File upload failed", error: err.code }),
      headers: {},
      statusCode: 400
    };
  }
};

//retrieves the boardmodel from the users file storage
module.exports.load = async (event, context) => {
  try {
    //retrieve the bearer token and validate it to get clientID
    const userID = await getClientID(event.headers.Authorization);
    if (userID.message !== undefined) {
      return {
        body: JSON.stringify({message: userID.message}),
        headers: {},
        statusCode: 400
      };
    }

    //parse the request body in 
    const fileInfo = JSON.parse(event.body);
    if (fileInfo.file == undefined || fileInfo.file == "" || fileInfo.model == undefined || fileInfo.model == ""){
      return {
        body: JSON.stringify({message: "File or Model field in request body undefined"}),
        headers: {},
        statusCode: 400
      };
    }
    const fileName = fileInfo.file;
    const modelName = fileInfo.model;

    //retrieve the file
    const result = await loadBoardFile(modelName, fileName, userID);
    console.log(result);
    if ("boardData" in result){
      return {
        body: JSON.stringify({message: "File load sucess", board: result.boardData}),
        headers: {},
        statusCode: 200
      };
    }
    return {
      body: JSON.stringify({message: "File load failed, Internal Server Error, please report", error: err.code }),
      headers: {},
      statusCode: 500
    };
    

  } catch(err) {
    return {
      body: JSON.stringify({message: "File load failed", error: err.code }),
      headers: {},
      statusCode: 400
    };
  }
}

//retrieves a list of models the user has stored
module.exports.get_models = async (event, context) => {
  try {
    //retrieve the bearer token and validate it to get clientID
    const userID = await getClientID(event.headers.Authorization);
    if (userID.message !== undefined) {
      return {
        body: JSON.stringify({message: userID.message}),
        headers: {},
        statusCode: 400
      };
    }

    //retrieve the list of files
    const result = await getModels(userID);
    if (Array.isArray(result)) {
      return {
        body: JSON.stringify({message: "File list retrieval sucess", files: result}),
        headers: {},
        statusCode: 200
      };
    }
    return {
      body: JSON.stringify({message: "File list retrieval failed"}),
      headers: {},
      statusCode: 500
    };

  } catch(err) {
    return {
      body: JSON.stringify({message: "Get Files failed", error: err.code }),
      headers: {},
      statusCode: 400
    };
  }
}


//retrieves a list of board models the user has stored
module.exports.get_boards = async (event, context) => {
  try {
    //retrieve the bearer token and validate it to get clientID
    const userID = await getClientID(event.headers.Authorization);
    if (userID.message !== undefined) {
      return {
        body: JSON.stringify({message: userID.message}),
        headers: {},
        statusCode: 400
      };
    }
    //parse in the model parameter
    const modelName = event.pathParameters.model;

    //retrieve the list of files
    const result = await getBoardFiles(userID, modelName);
    if (Array.isArray(result)) {
      return {
        body: JSON.stringify({message: "File list retrieval sucess", files: result}),
        headers: {},
        statusCode: 200
      };
    }
    return {
      body: JSON.stringify({message: "File list retrieval failed"}),
      headers: {},
      statusCode: 500
    };

  } catch(err) {
    return {
      body: JSON.stringify({message: "Get Files failed", error: err.code }),
      headers: {},
      statusCode: 400
    };
  }
}

//deletes the board model given
module.exports.delete = async (event, context) => {
  try {
    //retrieve the bearer token and validate it to get clientID
    const userID = await getClientID(event.headers.Authorization);
    if (userID.message !== undefined) {
      return {
        body: JSON.stringify({message: userID.message}),
        headers: {},
        statusCode: 400
      };
    }
    
    //parse the request body in 
    const fileInfo = JSON.parse(event.body);
    if (fileInfo.file == undefined || fileInfo.file == "" || fileInfo.model == undefined || fileInfo.model == ""){
      return {
        body: JSON.stringify({message: "File or Model field in request body undefined"}),
        headers: {},
        statusCode: 400
      };
    }
    const fileName = fileInfo.file;
    const modelName = fileInfo.model;

    //delete the file
    const result = await deleteBoardFile(modelName, fileName, userID);
    if (result !== "Failed" ) {
      return {
        body: JSON.stringify({message: result}),
        headers: {},
        statusCode: 200
      };
    }
    return {
      body: JSON.stringify({message: "File delete failed"}),
      headers: {},
      statusCode: 500
    };

  } catch(err) {
    return {
      body: JSON.stringify({message: "Delete File failed", error: err.code }),
      headers: {},
      statusCode: 400
    };
  }
}
